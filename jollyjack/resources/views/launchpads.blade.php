@extends('layouts.noFooter')
@section("customJS")

    {{--    <script src="{{asset("js/jquery.keyframes.min.js")}}" defer></script>--}}
    <script src="{{asset("js/buttonLpad.js")}}" defer></script>

@endsection
@section("customCSS")
    <link rel="stylesheet" href="{{asset("css/lpad.css")}}">
@endsection

@section('content')
    <div id="larea">
        <div id="editArea">
            <ul id="acordeon">
                <li>
                    <a class="toggle" href="javascript:void(0);">My Launchpads</a>
                    <ul class="inner">
                        <li>
                            <a class="toggle" href="#">Gests</a>
                            <ul class="inner">
                                <li></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="toggle" href="javascript:void(0);">Buttons</a>
                    <ul class="inner">
                        <li>
                            <a class="toggle" href="#">Layer 1</a>
                            <ul class="inner">
                                <li></li>
                            </ul>
                            <a class="toggle" href="#">Layer 2</a>
                            <ul class="inner">
                                <li></li>
                            </ul>
                            <a class="toggle" href="#">Layer 3</a>
                            <ul class="inner">
                                <li>
                                    <a class="toggle" href="#">Btn 15</a>
                                    <a class="toggle" href="#">Btn 16</a>
                                    <a class="toggle" href="#">Btn 17</a>
                                    <div class="inner">
                                        <p>
                                            <a> Color: #Blue</a>
                                        </p>
                                        <p>
                                            <a> Track: bass.mp3</a>
                                        </p>
                                        <p>
                                            <a> Start: Kepress</a>
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>

        </div>
        <div id="btnArea">
            <div id="launchpad">
                <div id="layers">
                    <div>
                        <div id="flayer1">
                            <div id="f1" class="fbutton"><a class="aux"></a></div>
                            <div id="f2" class="fbutton"><a class="aux"></a></div>
                            <div id="f3" class="fbutton"><a class="aux"></a></div>
                            <div id="f4" class="fbutton"><a class="aux"></a></div>
                            <div id="f5" class="fbutton"><a class="aux"></a></div>
                        </div>
                        <div id="layer1" class="layer">

                            @foreach( $test["first"] as $button)
                                <div id="{{$button["id"]}}" class="lpad__button" data-key="{{$button["key"]}}"
                                     data-track="{{$button["track"]}}" data-play="{{$button["play"]}}"
                                     data-end="{{$button["end"]}}" data-color="{{$button["color"]}}" data-loop="{{$button["loop"]}}">
                                </div>
                            @endforeach
                        </div>
                        <div id="layer2" class="layer">
                            @foreach( $test["second"] as $button)
                                <div id="{{$button["id"]}}" class="lpad__button" data-key="{{$button["key"]}}"
                                     data-track="{{$button["track"]}}" data-play="{{$button["play"]}}"
                                     data-end="{{$button["end"]}}" data-color="{{$button["color"]}}" data-loop="{{$button["loop"]}}">
                                </div>
                            @endforeach
                        </div>
                        <div id="layer3" class="layer">
                            @foreach( $test["third"] as $button)
                                <div id="{{$button["id"]}}" class="lpad__button" data-key="{{$button["key"]}}"
                                     data-track="{{$button["track"]}}" data-play="{{$button["play"]}}"
                                     data-end="{{$button["end"]}}" data-color="{{$button["color"]}}" data-loop="{{$button["loop"]}}">
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div id="vertical">
                        <div>
                            <div id="flayer2">
                                <div id="f6" class="fbutton"><a class="aux"></a></div>
                                <div id="f7" class="fbutton"><a class="aux"></a></div>
                                <div id="f8" class="fbutton"><a class="aux"></a></div>
                                <div id="f9" class="fbutton"><a class="aux"></a></div>
                                <div id="f10" class="fbutton"><a class="aux"></a></div>
                                <div id="f11" class="fbutton"><a class="aux"></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

