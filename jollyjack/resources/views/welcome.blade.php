@extends('layouts.app')

@section("customCSS")
    <link rel="stylesheet" href="{{asset("css/welcome.css")}}">
@endsection

@section("content")
    <header id="gtco-header" class="gtco-cover" role="banner">
        <div class="gtco-container">
            <div class="row">
                <div class="col-md-12 col-md-offset-0 text-left">
                    <div class="display-t">
                        <div class="display-tc">
                            <div class="row">
                                <div class="col-md-5 text-center header-img animate-box">
                                    <img src="images/logo-white.png" alt="">
                                </div>
                                <div class="col-md-7 copy animate-box">
                                    <h1>Online Launchpad</h1>
                                    <p>Sample, Create and Customize your music.</p>
                                    <p><a href="{{ route('launchpads') }}" target="_blank" class="btn btn-white">Get Started</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="hover-table-layout" >
        <div class="listing-item" >
            <figure class="image">
                <img src="https://i.ytimg.com/vi/MTrzTABzLfY/maxresdefault.jpg" alt="image">
                <figcaption>
                    <div class="caption">
                        <h1>$5</h1>
                        <p>Minumum</p>
                    </div>
                </figcaption>
            </figure>
            <div class="listing">
                <h4>Show catalogue listing</h4>
                <h4>Email signatures and banners</h4>
                <h4>E-invitations</h4>
                <h4>Online brochures</h4>
            </div>
        </div>
        <div class="listing-item">
            <figure class="image">
                <img src="https://i.ytimg.com/vi/MTrzTABzLfY/maxresdefault.jpg" alt="image">
                <figcaption>
                    <div class="caption">
                        <h1>$10</h1>
                        <p>Medium</p>
                    </div>
                </figcaption>
            </figure>
            <div class="listing">
                <h4>Press releases</h4>
                <h4>Company logos for online listings</h4>
                <h4>Product categories</h4>
                <h4>Using the ADIPEC logo on exhibitor advertising</h4>
                <h4>Commercial opportunities in the preview and show dailies</h4>
            </div>
        </div>
        <div class="listing-item">
            <figure class="image">
                <img src="https://i.ytimg.com/vi/MTrzTABzLfY/maxresdefault.jpg" alt="image">
                <figcaption>
                    <div class="caption">
                        <h1>$20</h1>
                        <p>Big</p>
                    </div>
                </figcaption>
            </figure>
            <div class="listing">
                <h4>Mobile app listing</h4>
                <h4>Order visitor badges</h4>
                <h4>Global meetings programme</h4>
                <h4>Get Social</h4>
            </div>
        </div>
    </div>
@endsection

