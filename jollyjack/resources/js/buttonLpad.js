async function playButton(button){
    if(button.stop == button.playEvent ){
        await sleep(10);
        button.playEvent = "none";
    }

    button.audio.play();
    button.playing = true;
    $("#"+button.id).css({ "box-shadow": "0 0.5em 0.5em -0.4em "+button.color , "transform": "translateY(-0.25em)"});
}
async function stopButton(button) {
    button.audio.pause();
    button.audio = new Audio(button.track);
    button.playing = false;
    $("#"+button.id).css({ "box-shadow": "none", "transform": "translateY(0em)"});

    if( button.playEvent == "none"){
        await sleep(10);
        button.playEvent = button.stop
    }

}
// function to lighten a color
const addLight = function(color, amount){
    let cc = parseInt(color,16) + amount;
    let c = (cc > 255) ? 255 : (cc);
    c = (c.toString(16).length > 1 ) ? c.toString(16) : `0${c.toString(16)}`;
    return c;
};
const lighten = (color, amount)=> {
    color = (color.indexOf("#")>=0) ? color.substring(1,color.length) : color;
    amount = parseInt((255*amount)/100);
    return color = `#${addLight(color.substring(0,2), amount)}${addLight(color.substring(2,4), amount)}${addLight(color.substring(4,6), amount)}`;
};
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


function eventSetter(){

    //this events could be put inside a loop or a function
    $(document).on('keydown',function(e) {
        audioRecolection.forEach(function (item) {
            if(e.which == item.key) {
                if(item.stop == 'keydown'&& item.playing){
                    stopButton(item);
                } else if(item.playEvent == 'keydown'){

                    playButton(item);
                }
            }
        });

    });

    $(document).on('keyup',function(e) {
        audioRecolection.forEach(function (item) {
            if(e.which == item.key) {
                if(item.stop == 'keyrelease'&& item.playing){
                    stopButton(item);
                }
                else if(item.playEvent == 'keyrelease'){
                    playButton(item);
                }
            }
        });

    });
    $(document).on('keypress',function(e) {
        audioRecolection.forEach(function (item) {
            if(e.which == item.key) {
                if(item.stop == 'keypress'&& item.playing){
                    stopButton(item);

                }
                else if(item.playEvent == 'kepress'){
                    playButton(item);
                }
            }
        });

    });
}
let audioRecolection = [];


$(".lpad__button").each(function () {
    let audio = new Audio('sounds/'+ $(this).data("track"));
    // console.log($(this).data("loop"));
    // if ($(this).data("loop") === "true"){
    //     console.log("hola")
    // }
    // audio.loop = $(this).data("loop");


    //tried to simplify but it stoped workin
    audio.loop = true;

    //possible en rgba y en hex
    // $(this).css({"background-color": lighten($(this).data("color"), 25), "border-color": $(this).data("color")});
    $(this).css({"background-color":$(this).data("color") , "border-color": lighten($(this).data("color"), 50)});
    audioRecolection.push( {"id" : $(this).attr('id'),"track" : 'sounds/'+ $(this).data("track"), "audio" : audio, "playing": false, "key":$(this).data("key") , "playEvent": $(this).data("play"), "stop":$(this).data("end"),"color": $(this).data("color")});
    eventSetter();
});


$(function () {     $('.aux').prepend('<div class="hover"><span></span><span></span><span></span><span></span></div>'); });
$('.toggle').click(function(e) {     e.preventDefault();      var $this = $(this);      if ($this.next().hasClass('show')) {         $this.next().removeClass('show');         $this.next().slideUp(350);     } else {         $this.parent().parent().find('li .inner').removeClass('show');         $this.parent().parent().find('li .inner').slideUp(350);         $this.next().toggleClass('show');         $this.next().slideToggle(350);     } });
