<?php


namespace App\Http\Controllers;
use App\Lpad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LaunchpadController  extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //uncoment to force login to enter
//        $this->middleware('auth');
}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        Lpad::forceCreate(["user_id" => 0,"name" => "Test",
            "buttons" =>
//                '{"first" :[{"id": 1, "key":65,"play":"keydown","track":"test.wav","end":"keyrelease","color":"#000", "loop": "true"},{"id": 2,"key":70,"play":"keydown","track":"test.wav","end":"keyrelease","color":"#000", "loop": "false"}]}'
        '
{
    "first": [
        {
            "id": 1,
            "key": 81,
            "play": "keydown",
            "track": "link-rosa-1f-2c-A.mp3",
            "end": "keyrelease",
            "color": "#f42ee0",
            "loop": "true"
        },
        {
            "id": 2,
            "key": 87,
            "play": "keydown",
            "track": "link-rosa-2f-1c-A.mp3",
            "end": "keyrelease",
            "color": "#f42ee0",
            "loop": "true"
        },
        {
            "id": 3,
            "key": 69,
            "play": "keydown",
            "track": "link-rosa-2f-2c-A.mp3",
            "end": "keyrelease",
            "color": "#f42ee0",
            "loop": "true"
        },
        {
            "id": 4,
            "key": 82,
            "play": "keydown",
            "track": "link-rosa-2f-3c-A.mp3",
            "end": "keyrelease",
            "color": "#f42ee0",
            "loop": "true"
        },
        {
            "id": 21,
            "key": 73,
            "play": "keydown",
            "track": "link-rosa-1f-2c-B.mp3",
            "end": "keyrelease",
            "color": "#f42ee0",
            "loop": "true"
        },        {
            "id": 22,
            "key": 79,
            "play": "keydown",
            "track": "link-rosa-2f-2c-B.mp3",
            "end": "keyrelease",
            "color": "#f42ee0",
            "loop": "true"
        },        {
            "id": 23,
            "key": 80,
            "play": "keydown",
            "track": "link-rosa-3f-2c-B.mp3",
            "end": "keyrelease",
            "color": "#f42ee0",
            "loop": "true"
        },        {
            "id": 24,
            "key": 186,
            "play": "keydown",
            "track": "link-rosa-4f-2c-B.mp3",
            "end": "keyrelease",
            "color": "#f42ee0",
            "loop": "true"
        },
        {
            "id": 5,
            "key": 84,
            "play": "keydown",
            "track": "link-naranja-4f-1c-A.mp3",
            "end": "keyrelease",
            "color": "#f29532",
            "loop": "true"
        },
        {
            "id": 6,
            "key": 89,
            "play": "keydown",
            "track": "link-naranja-4f-3c-A.mp3",
            "end": "keyrelease",
            "color": "#f29532",
            "loop": "true"
        },
        {
            "id": 7,
            "key": 85,
            "play": "keydown",
            "track": "link-naranja-4f-2c-A.mp3",
            "end": "keyrelease",
            "color": "#f29532",
            "loop": "true"
        }

    ],
    "second": [
        {
            "id": 17,
            "key": 65,
            "play": "keydown",
            "track": "link-naranja-1f-3c-B.mp3",
            "end": "keyrelease",
            "color": "#f29532",
            "loop": "true"
        },
        {
            "id": 18,
            "key": 83,
            "play": "keydown",
            "track": "link-naranja-2f-3c-B.mp3",
            "end": "keyrelease",
            "color": "#f29532",
            "loop": "true"
        }  ,
        {
            "id": 19,
            "key": 68,
            "play": "keydown",
            "track": "link-naranja-3f-3c-B.mp3",
            "end": "keyrelease",
            "color": "#f29532",
            "loop": "true"
        } ,
        {
            "id": 20,
            "key": 70,
            "play": "keydown",
            "track": "link-naranja-4f-3c-B.mp3",
            "end": "keyrelease",
            "color": "#f29532",
            "loop": "true"
        },
        {
            "id": 8,
            "key": 71,
            "play": "keydown",
            "track": "link-verde-3f-1c-A.mp3",
            "end": "keyrelease",
            "color": "#5cea46",
            "loop": "true"
        },
        {
            "id": 9,
            "key": 72,
            "play": "keydown",
            "track": "link-verde-3f-2c-A.mp3",
            "end": "keyrelease",
            "color": "#5cea46",
            "loop": "true"
        },
        {
            "id": 10,
            "key": 74,
            "play": "keydown",
            "track": "link-verde-3f-3c-A.mp3",
            "end": "keyrelease",
            "color": "#5cea46",
            "loop": "true"
        } ,
        {
            "id": 14,
            "key": 75,
            "play": "keydown",
            "track": "link-green-2f-1c-B.mp3",
            "end": "keyrelease",
            "color": "#5cea46",
            "loop": "true"
        } ,
        {
            "id": 15,
            "key": 76,
            "play": "keydown",
            "track": "link-green-3f-1c-B.mp3",
            "end": "keyrelease",
            "color": "#5cea46",
            "loop": "true"
        },
        {
            "id": 16,
            "key": 192,
            "play": "keydown",
            "track": "link-green-4f-1c-B.mp3",
            "end": "keyrelease",
            "color": "#5cea46",
            "loop": "true"
        }
    ],
    "third": [
        {
            "id": 11,
            "key": 90,
            "play": "keydown",
            "track": "link-bucle-1f-1c-A.mp3",
            "end": "keydown",
            "color": "#3986e5",
            "loop": "true"
        },
        {
            "id": 12,
            "key": 88,
            "play": "keydown",
            "track": "link-bucle-1f-3c-A.mp3",
            "end": "keydown",
            "color": "#3986e5",
            "loop": "true"
        },
        {
            "id": 13,
            "key": 67,
            "play": "keydown",
            "track": "link-bucle-1f-1c-B.mp3",
            "end": "keydown",
            "color": "#3986e5",
            "loop": "true"
        }
    ]
}


        '
        ]);
//        auth()->

//        $var = Lpad::with("user")->where("user_id", 1)->get();
        $userID = Auth::id();
        if ($userID === null){
            $userID = 0;
        }
        $var = Lpad::where("user_id", $userID)->get("buttons")->toArray();
        $test = json_decode($var[0]["buttons"], true);
//        var_dump($test["first"][0]["loop"]);
        return view('launchpads', ["test" => $test]);
    }
}
