<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>JollyJack</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
{{--        <link rel="shortcut icon" href="{{ asset('img/favicon-16x16.png') }}">--}}
{{--        <link rel="icon" href="{{ URL::asset('img/favicon-16x16.png') }}" type="image/x-icon"/>--}}
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,700" rel="stylesheet">

        <!-- Animate.css -->
        <link rel="stylesheet" href="{{asset("css/animate.css")}}">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="{{asset("css/icomoon.css")}}">
        <!-- Themify Icons-->
        <link rel="stylesheet" href="{{asset("css/themify-icons.css")}}">
        <!-- Bootstrap  -->
        <link rel="stylesheet" href="{{asset("css/bootstrap.css")}}">

        <!-- Magnific Popup -->
        <link rel="stylesheet" href="{{asset("css/magnific-popup.css")}}">

        <!-- Owl Carousel  -->
        <link rel="stylesheet" href="{{asset("css/owl.carousel.min.css")}}">
        <link rel="stylesheet" href="{{asset("css/owl.theme.default.min.css")}}">

        <!-- Theme style  -->
        <link rel="stylesheet" href="{{asset("css/style.css")}}">
        @yield("customCSS")

        <!-- Modernizr JS -->
        <script src="{{asset("js/modernizr-2.6.2.min.js")}}"></script>



        <!-- jQuery -->
        <script src="{{asset("js/jquery.min.js")}}" defer></script>
        <!-- jQuery Easing -->
        <script src="{{asset("js/jquery.easing.1.3.js")}}" defer></script>
        <!-- Bootstrap -->
        <script src="{{asset("js/bootstrap.min.js")}}" defer></script>
        <!-- Waypoints -->
        <script src="{{asset("js/jquery.waypoints.min.js")}}" defer></script>
        <!-- Carousel -->
        <script src="{{asset("js/owl.carousel.min.js")}}" defer></script>
        <!-- Magnific Popup -->
        <script src="{{asset("js/jquery.magnific-popup.min.js")}}" defer></script>
        <script src="{{asset("js/magnific-popup-options.js")}}" defer></script>
        <!-- Main -->
        <script src="{{asset("js/main.js")}}" defer></script>
        @yield('customJS')
</head>
<body>
    <div id="app">
        <div class="gtco-loader"></div>
        <div id="page">
            <nav class="gtco-nav" role="navigation">
                <div class="gtco-container">

                    <div class="row">
                        <div class="col-sm-2 col-xs-12">
                            {{--                        LOGO --}}
                            <div id="gtco-logo"><a href="{{url('/')}}"><img src="{{asset('images/logo-notext.png')}}" alt="" style="width: 20%"></a> Jolly Jack</div>
                        </div>
                        <div class="col-xs-10 text-right menu-1">
                            <ul>
                                <li><a href="{{url('/')}}">Home</a></li>
                                <li

                                    @if(str_contains(url()->current(), '/launchpads'))
                                    class="active"
                                    @endif
                                ><a href="{{ route('launchpads') }}">Launchpads</a></li>
                                @guest
                                    <li

                                        @if(str_contains(url()->current(), '/login'))
                                        class="active"
                                        @endif
                                    >
                                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </li>
                                    @if (Route::has('register'))
                                        <li

                                            @if(str_contains(url()->current(), '/register'))
                                            class="active"
                                            @endif
                                        >
                                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                        </li>
                                    @endif
                                @else
                                    <li class="dropdown">
                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                    </li>
                                @endguest
                                {{--                            <li class="has-dropdown">--}}
                                {{--                                <a href="services.html">Services</a>--}}
                                {{--                                <ul class="dropdown">--}}
                                {{--                                    <li><a href="#">Web Design</a></li>--}}
                                {{--                                    <li><a href="#">eCommerce</a></li>--}}
                                {{--                                    <li><a href="#">Branding</a></li>--}}
                                {{--                                    <li><a href="#">API</a></li>--}}
                                {{--                                </ul>--}}
                                {{--                            </li>--}}
                                {{--                            <li class="has-dropdown">--}}
                                {{--                                <a href="#">Dropdown</a>--}}
                                {{--                                <ul class="dropdown">--}}
                                {{--                                    <li><a href="#">HTML5</a></li>--}}
                                {{--                                    <li><a href="#">CSS3</a></li>--}}
                                {{--                                    <li><a href="#">Sass</a></li>--}}
                                {{--                                    <li><a href="#">jQuery</a></li>--}}
                                {{--                                </ul>--}}
                                {{--                            </li>--}}
                                {{--                            <li><a href="portfolio.html">Portfolio</a></li>--}}
                                {{--                            <li><a href="contact.html">Contact</a></li>--}}
                            </ul>
                        </div>
                    </div>

                </div>
            </nav>
            <main class="py-4">
                @yield('content')

            </main>
            @yield('footer')
        </div>


    </div>
</body>
</html>
